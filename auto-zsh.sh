#! /bin/bash


if [ -f /etc/os-release ]; then
    # freedesktop.org and systemd
    . /etc/os-release
    OS=$ID
    VER=$VERSION_ID
fi
echo $OS
echo $VER
fedora=fedora
ubuntu=ubuntu
debian=debian

if [[ "$OS" == "fedora" ]]; then
	echo "Fedora OS type detected"
	echo "Installing ZSH with dnf (will require sudo privileges)"
	sudo dnf install -y zsh
elif [[ "$OS" == "debian" ]] || [[ "$OS" == "ubuntu" ]]; then
	echo "Debian based OS type detected"
	echo "Installing ZSH with apt"
	sudo apt install -y zsh
	echo "Installing curl, wget and git"
	sudo apt install -y curl wget git
else
	echo "Could not recognise OS type, aborting"
	exit 1
fi


echo "ZSH is now installed, will execute oh-my-zsh script, follow its instructions"
wget -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh
./install.sh
echo "oh-my-zsh installed"
echo "Cloning extensions and theme from their respective folders"
echo "Cloning zsh-syntax-highlighting"
git clone https://github.com/zsh-users/zsh-syntax-highlighting
echo "Cloning zsh-autosuggestions"
git clone https://github.com/zsh-users/zsh-autosuggestions
echo "Cloning zsh-completions"
git clone https://github.com/zsh-users/zsh-completions
echo "Cloning powerlevel10k"
git clone https://github.com/romkatv/powerlevel10k
echo "Downloading needed fonts for powerlevel10k using wget"
wget https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Regular.ttf https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Bold.ttf https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Italic.ttf https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Bold%20Italic.ttf
echo "Fonts downloaded, moving to ~/.local/share/font"
if [ ! -d "~/.local/share/fonts"]; then # if the folder does not exists, creating it
	mkdir ~/.local/share/fonts
fi
mv MesloLGS* ~/.local/share/fonts
echo "Fonts moved, will probably need to log out/in for taking it into account"
echo "Moving ZSH plugins and theme"
mv zsh* .oh-my-zsh/plugins
mv powerlevel10k .oh-my-zsh/themes
echo "Files moved, now renaming default .zshrc to .zshrc.default and downloading custom one"
wget https://gitlab.enssat.fr/public-repo/auto-zsh/-/raw/main/.zshrc\?ref_type\=heads
echo "Moving .zshrc and sourcing it, powerlevel10k config should show up, if there is an issue with the fonts, press q to cancel p10k config, reboot, then run 'p10k configure' to restart the configuration of p10k"
mv .zshrc\?ref_type=heads ~/.zshrc
source ~/.zshrc
echo "Done !"
